#pragma once
#include <spring\Framework\IScene.h>
#include <qobject.h>
#include <qcustomplot.h>

namespace Spring
{
   class SinesScene :public IScene
   {
      Q_OBJECT
   public:
      SinesScene(const std::string& ac_szSceneName);
      ~SinesScene();

      virtual void createScene() override;

      virtual void release() override;

   public slots:
      void mp_OkButton();

   private:
      QWidget * centralWidget;
      QLineEdit* sine1;
      QLineEdit* sine2;
      QLineEdit* sine3;
      QLineEdit* sine4;
      QLineEdit* sine5;
   };
}


