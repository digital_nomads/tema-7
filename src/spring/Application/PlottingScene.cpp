#include <spring\Application\PlottingScene.h>

#include "ui_plotting_scene.h"
#include "aquila/aquila.h"
#include "Iir.h"

namespace Spring
{
	PlottingScene::PlottingScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void PlottingScene::createScene()
	{
		// Create the UI
		const auto ui = std::make_shared<Ui_plottingScene>();
		ui->setupUi(m_uMainWindow.get());

    // Connect btn's release signal to defined slot
    QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mp_BackButton()));

		// Setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("Plotting Example"));

    mv_customPlotTime = ui->customPlotTime;
    mv_customPlotFreq = ui->customPlotFreq;

    // Connect btn's release signal to defined slot
    QObject::connect(ui->plotButton, SIGNAL(released()), this, SLOT(mp_OnPlotButton()));
    QObject::connect(ui->exportButton, SIGNAL(released()), this, SLOT(mp_ExportButton()));



		// Setting centralWidget
		centralWidget = ui->centralwidget;

    // Populate combobox
    mv_signalComboBox = ui->SignalComboBox;
    mv_signalComboBox->addItem("Sine");
    mv_signalComboBox->addItem("SineSum");
    mv_signalComboBox->addItem("Square");
    mv_signalComboBox->addItem("Sweep");
    mv_signalComboBox->addItem("Dirac");
    mv_signalComboBox->addItem("Sine sum filtered");
    mv_signalComboBox->addItem("Sawtooth");
    mv_signalComboBox->addItem("5 sines");
    mv_signalComboBox->addItem("AmplitudeModulation");

    //QObject::connect(mv_signalComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(mp_FiveSines(int)));
    mv_carrierFreq = ui->lineEditWaveFrequency;
    // Initialize plotters
    mp_InitPlotters();

    // We dont want to export if nothing was plotted 
    m_TransientDataCollection["WasPlotted"] = false;
	}

  void PlottingScene::mp_InitPlotters()
  {
    // Time plot
    mv_customPlotTime->setInteraction(QCP::iRangeZoom, true);
    mv_customPlotTime->setInteraction(QCP::iRangeDrag, true);

    mv_customPlotTime->addGraph();
    mv_customPlotTime->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
    
    mv_customPlotTime->xAxis->setLabel("SAMPLES");
    mv_customPlotTime->yAxis->setLabel("AMPLITUDE");

    // Freq plot
    mv_customPlotFreq->setInteraction(QCP::iRangeZoom, true);
    mv_customPlotFreq->setInteraction(QCP::iRangeDrag, true);

    mv_customPlotFreq->addGraph();
    mv_customPlotFreq->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);

    mv_customPlotFreq->xAxis->setLabel("FREQUENCY (Hz)");
    mv_customPlotFreq->yAxis->setLabel("AMPLITUDE");
    mv_customPlotFreq->xAxis->setRange(0, 1000);
    mv_customPlotFreq->yAxis->setRange(-0.2, 2);
  }

  void PlottingScene::release()
	{
		delete centralWidget;
	}

	PlottingScene::~PlottingScene()
	{
	}

  double SineWave(double t, double amplitude, double frequency)
  {
    return amplitude * sin(2 * M_PI * frequency * t);
  }

  double ModulationWave(double t, double amplitude, double frequency)
  {
     return amplitude*cos(2 * M_PI*frequency*t);
  }

 
  template <typename T> int sgn(T val)
  {
     return (T(0) < val) - (val < T(0));
  }

  double Sweep(double f_start, double f_end, double interval, int steps, double x)
  {
     double delta = x / (float)steps;
     double t = interval*delta;
     double phase = 2 * std::_Pi*t*(f_start + (f_end - f_start)*delta / 2);
     return 3 * sin(phase);
  }
  double SawToothWave(double t, double tootNr)
  {
     return t * tootNr - floor(t * tootNr);
  }

  void PlottingScene::mp_BackButton()
  {
    const std::string c_szNextSceneName = "Initial scene";
    emit SceneChange(c_szNextSceneName);
  }

  static const unsigned int SAMPLE_RATE = 81920; // Hz
  static const       double PLOT_TIME = 0.1;     // seconds
  static const       double AMPLITUDE = 1;

  static const unsigned int NO_OF_SAMPLES = PLOT_TIME * SAMPLE_RATE;

  void PlottingScene::mp_OnPlotButton()
  {
     m_TransientDataCollection["WasPlotted"] = true;
     if (m_TransientDataCollection.find("5sines") == m_TransientDataCollection.end()) {
        m_TransientDataCollection["5sines"] = false;
     }

     QString comboSelection = mv_signalComboBox->currentText();

     // Set xAxis
     QVector<double> y(NO_OF_SAMPLES);
     QVector<double> x(NO_OF_SAMPLES);
     x[0] = 0;
     for (int i = 1; i < NO_OF_SAMPLES; i++) {
        x[i] = i;
     }

     if (comboSelection == "Sine") {
        // Set yAxis
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           double xval = i / (double)NO_OF_SAMPLES;
           y[i] = SineWave(xval, AMPLITUDE, 20);
        }
     } else if (comboSelection == "SineSum") {
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           double xval = i / (double)NO_OF_SAMPLES;
           y[i] = SineWave(xval, AMPLITUDE, 20) + SineWave(xval, AMPLITUDE / 20, 200);
        }
     } else if (comboSelection == "Square") {
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           double xval = i / (double)NO_OF_SAMPLES;
           double val = SineWave(xval, AMPLITUDE, 20);
           y[i] = sgn<double>(SineWave(xval, AMPLITUDE, 20));
        }
     } else if (comboSelection == "Sweep") {
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           double xval = i / (double)NO_OF_SAMPLES;
           double val = SineWave(xval, AMPLITUDE, 20);
           y[i] = sgn<double>(SineWave(xval, AMPLITUDE, 20));
        }
     } else if (comboSelection == "Dirac") {
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           double xval = i / (double)NO_OF_SAMPLES;
           y[i] = 0;
        }
        y[1000] = 1000;
     } else if (comboSelection == "Sine sum filtered") {
        Iir::Butterworth::LowPass<4> f;
        const float samplingrate = SAMPLE_RATE; // Hz
        const float cutoff_frequency = 500; // Hz
        f.setup(4, samplingrate, cutoff_frequency);

        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           double xval = i / (double)NO_OF_SAMPLES;
           double yval1 = SineWave(xval, AMPLITUDE, 100);
           double yval2 = SineWave(xval, AMPLITUDE / 10, 440);
           double yval3 = SineWave(xval, AMPLITUDE / 2, 670);
           double yval4 = SineWave(xval, AMPLITUDE / 5, 800);
           double yval5 = SineWave(xval, AMPLITUDE / 13, 1200);
           double val = yval1 + yval2 + yval3 + yval4 + yval5;
           y[i] = f.filter(val);
        }

     } else if (comboSelection == "Sawtooth") {
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           double xval = i / (double)NO_OF_SAMPLES;
           y[i] = SawToothWave(xval, 6);
        }
     } else if (comboSelection == "5 sines") {

        bool fiveSines = boost::any_cast<bool>(m_TransientDataCollection.find("5sines")->second);
        if (fiveSines) {
           m_TransientDataCollection["5sines"] = false;

           std::vector<double> freq = boost::any_cast<std::vector<double>>(m_TransientDataCollection.find("Frequencies")->second);
           for (int i = 0; i < NO_OF_SAMPLES; i++) {
              double xval = i / (double)NO_OF_SAMPLES;
              double yval1 = SineWave(xval, AMPLITUDE, freq[0]);
              double yval2 = SineWave(xval, AMPLITUDE, freq[1]);
              double yval3 = SineWave(xval, AMPLITUDE, freq[2]);
              double yval4 = SineWave(xval, AMPLITUDE, freq[3]);
              double yval5 = SineWave(xval, AMPLITUDE, freq[4]);
              double val = yval1 + yval2 + yval3 + yval4 + yval5;
              y[i] = val;
           }

        } else {
           m_TransientDataCollection["5sines"] = true;
           const std::string c_szNextSceneName = "Sines scene";
           emit SceneChange(c_szNextSceneName);
        }
     } else if (comboSelection == "AmplitudeModulation") {
        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           double xval = i / (double)NO_OF_SAMPLES;
           double freq = mv_carrierFreq->text().toDouble();
           y[i] = (1 + ModulationWave(xval, -AMPLITUDE, 5))*SineWave(xval, AMPLITUDE, freq);
        }
     }
     // FFT

     bool fiveSines = boost::any_cast<bool>(m_TransientDataCollection.find("5sines")->second);
     if (fiveSines == false) {

        QVector<double> yFFT(NO_OF_SAMPLES);

        auto fft = Aquila::FftFactory::getFft(NO_OF_SAMPLES);
        Aquila::SpectrumType spectrum = fft->fft(y.data());

        for (int i = 0; i < NO_OF_SAMPLES; i++) {
           yFFT[i] = std::abs(spectrum[i]) / (NO_OF_SAMPLES / 2);
        }

        mv_customPlotTime->graph(0)->setData(x, y);
        mv_customPlotTime->rescaleAxes();
        mv_customPlotTime->replot();

        mv_customPlotFreq->graph(0)->setData(x, yFFT);
        mv_customPlotFreq->replot();

        // make a copy because signal source cant use qvector<qreal> container;
        std::vector<double> y2(NO_OF_SAMPLES);
        for (int i = 0; i < y.size(); i++) {
           y2[i] = y[i];
        }
        m_TransientDataCollection["SignalSource"] = Aquila::SignalSource(y2, 100);
     }

  }
  void PlottingScene::mp_ExportButton()
  {

     bool wasPlotted= boost::any_cast<bool>(m_TransientDataCollection.find("WasPlotted")->second);
     if (wasPlotted) {
        QString comboSelection = mv_signalComboBox->currentText();
        Aquila::SignalSource signalSource= boost::any_cast<Aquila::SignalSource>(m_TransientDataCollection.find("SignalSource")->second);
        Aquila::WaveFile::save(signalSource, comboSelection.toStdString()+".wav");
     }
     
  }
  void PlottingScene::mp_FiveSines(int index)
  {
     if (index == 8) {
        const std::string c_szNextSceneName = "Sines scene";
        emit SceneChange(c_szNextSceneName);
     }

  }

}
