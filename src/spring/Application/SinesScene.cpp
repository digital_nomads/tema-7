#include "spring\Application\SinesScene.h"
#include "ui_sines_scene.h"


namespace Spring
{
   SinesScene::SinesScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
   {}


   SinesScene::~SinesScene()
   {}

   void SinesScene::createScene()
   {
      const auto ui = std::make_shared<Ui_sinesScene>();
      ui->setupUi(m_uMainWindow.get());
      centralWidget = ui->centralwidget;

      QObject::connect(ui->pushButtonOk, SIGNAL(released()), this, SLOT(mp_OkButton()));

      sine1 = ui->lineEditSine1;
      sine1->setValidator(new QDoubleValidator(0, 2000, 10, this));
      sine2 = ui->lineEditSine2;
      sine2->setValidator(new QDoubleValidator(0, 2000, 10, this));
      sine3 = ui->lineEditSine3;
      sine3->setValidator(new QDoubleValidator(0, 2000, 10, this));
      sine4 = ui->lineEditSine4;
      sine4->setValidator(new QDoubleValidator(0, 2000, 10, this));
      sine5 = ui->lineEditSine5;
      sine5->setValidator(new QDoubleValidator(0, 2000, 10, this));
   }

   void SinesScene::release()
   {
      delete centralWidget;
   }

   void SinesScene::mp_OkButton()
   {
      std::vector<double> frequencies(5);
      frequencies[0]= sine1->text().toDouble();
      frequencies[1] = sine2->text().toDouble();
      frequencies[2] = sine3->text().toDouble();
      frequencies[3] = sine4->text().toDouble();
      frequencies[4] = sine5->text().toDouble();

      m_TransientDataCollection["Frequencies"] = frequencies;
      m_TransientDataCollection["5sines"] = true;

      const std::string c_szNextSceneName = "Plotting scene";
      emit SceneChange(c_szNextSceneName);
   }
}

